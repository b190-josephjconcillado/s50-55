import React from "react";

/**
 * Creates UserContext object
 *
 * createContext method allows us to store a different data inside an object, in this case called UserContext this set of information can be shared to
 * other components within the app
 */

const UserContext = React.createContext();

/**
 * Provider component allows other to consume /use the context object and supply the necessary information needed to the context object
 */
// export without default needs destructuring before being able to be imported to other components
export const UserProvider = UserContext.Provider;

export default UserContext;
