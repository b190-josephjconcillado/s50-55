import { Card } from "react-bootstrap";
// import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function CourseCard({ courseProp }) {
  // console.log(courseProp);
  // console.log(typeof courseProp);

  const { name, price, _id } = courseProp;

  // const [count,setCount] = useState(0);
  // const [seat, setSeat] = useState(30);
  // const [disable, setDisable] = useState(false);
  // const [isOpen, setIsOpen] = useState(true);

  // function enroll() {
  //     if(seat !== 0) {
  //         setSeat(seat - 1);
  //         setCount(count + 1);
  //     } else {
  //         setDisable(true);
  //         alert ('No more seats.');
  //     }
  // }

  // function enroll() {
  //         setSeat(seat - 1);
  //         setCount(count + 1);
  //         // setDisable(true);
  // }
  // useEffect(() => {
  //     if(seat === 0) {
  //         setIsOpen(false);
  //     }
  // },[seat]);

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        {/* <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text> */}
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        {/* <Card.Text>Enrollees: {count}</Card.Text> */}
        {/* {
                    isOpen ? <Button variant="primary" onClick={enroll} >Enroll</Button>
                    :
                    <Button variant="primary" onClick={enroll} disabled>Enroll</Button>
                } */}
        <Link className="btn btn-primary" to={`/courses/${_id}`}>
          Details
        </Link>
      </Card.Body>
    </Card>
  );
}
