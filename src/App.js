import "./App.css";
import { UserProvider } from "./UserContext";
import { useEffect, useState } from "react"; //Fragment
import AppNavbar from "./AppNavbar";
import { Container } from "react-bootstrap";
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Error from "./pages/ErrorPage";
import CourseView from "./components/CourseView";

function App() {
  // const [user, setUser] = useState({
  //   email: localStorage.getItem("email"),
  // });

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    token: localStorage.getItem("token"),
  });

  const unSetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);
  // console.log(user);
  return (
    <UserProvider value={{ user, setUser, unSetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element={<CourseView />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
