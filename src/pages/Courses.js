// import coursesData from '../data/coursesData.js';
import CourseCard from "../components/CourseCard.js";
import { Fragment, useEffect, useState } from "react";

export default function Courses() {
  // console.log(coursesData);

  const [courses, setCourses] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} courseProp={course} />;
          })
        );
      });
  }, []);
  // const courses = coursesData.map(course => {
  //     return (
  //         <CourseCard key={course.id} courseProp={course}/>
  //     )
  // });

  return <Fragment>{courses}</Fragment>;
}
