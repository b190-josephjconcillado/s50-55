import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom"; //useNavigate
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [disable, isDisable] = useState(true);

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
        // localStorage.setItem("id", data._id);
        // localStorage.setItem("isAdmin", data.isAdmin);
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      isDisable(false);
    } else {
      isDisable(true);
    }
    if (user.id !== null) {
      navigate("/courses");
    }
  }, [email, password, user, navigate]);

  function loginUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: "Login successful!",
            icon: "success",
            text: "Welcome to Zuitt!",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your login credentials and try again.",
          });
        }
      });

    // localStorage.setItem("email", email);
    // setUser({
    //   email: email,
    // });
    setEmail("");
    setPassword("");
    // navigate("/");
    // window.location.reload(false);
    // alert("You are now logged in.");

    // let emailIndex = usersData.findIndex(user => user.email === email);

    // if(emailIndex > -1 && (email === usersData[emailIndex].email && password === usersData[emailIndex].password)) {
    //     setEmail('');
    //     setPassword('');
    //     alert('You are now logged in.');
    // } else {
    //     alert('Invalid email/password');
    // }
  }

  console.log(` whats up yow ${user.id}`);

  return user.token !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(e) => loginUser(e)}>
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
      </Form.Group>
      <Button variant="primary" type="submit" id="submitBtn" disabled={disable}>
        Submit
      </Button>
      {/* {
                isActive ? <Button variant="primary" type="submit" id='submitBtn'>Submit</Button>
                :
                <Button variant="primary" type="submit" id='submitBtn' disabled>Submit</Button>
            } */}
    </Form>
  );
}
