import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";

export default function Logout() {
  const { setUser, unSetUser } = useContext(UserContext);

  useEffect(() => {
    setUser({ id: null, isAdmin: null, token: null });
    unSetUser();
  });

  return <Navigate to="/login" />;
}
