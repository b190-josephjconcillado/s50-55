import { Link } from 'react-router-dom';
import { Row } from 'react-bootstrap';

export default function ErrorPage() {

	return(
		<Row>
			<h1>Page Not Found</h1>
			<p>Go back to <Link to='/'>homepage</Link></p>
		</Row>

	)
}
