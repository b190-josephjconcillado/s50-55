import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [disable, setDisable] = useState(true);
  console.log(
    `${firstName} ${lastName} ${email} ${mobileNumber} ${password1} ${password2}`
  );

  useEffect(() => {
    if (
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      firstName !== "" &&
      lastName !== "" &&
      mobileNumber !== ""
    ) {
      if (password1 === password2 && mobileNumber.length > 11) {
        setDisable(false);
        //setIsActive(false);
      } else {
        setDisable(true);
        //setIsActive(true);
      }
    }
    if (user.token !== null) {
      navigate("/courses");
    }
  }, [
    email,
    password1,
    password2,
    user,
    navigate,
    firstName,
    lastName,
    mobileNumber,
  ]);

  //   useEffect(() => {
  //     if (user.email !== null) {
  //       navigate("/courses");
  //     }
  //   });

  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNumber,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: "Registration successful",
            icon: "success",
            text: "Welcome to Zuitt!",
          });
          navigate("/login");
          setEmail("");
          setPassword1("");
          setPassword2("");
          navigate("/login");
        } else {
          Swal.fire({
            title: "Duplicate email found.",
            icon: "error",
            text: "Please provide a different email.",
          });
        }
      });
  }

  return (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group className="mb-3" controlId="userFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="userLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>
      <Form.Group className="mb-3" controlId="userMobileNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter mobile number"
          value={mobileNumber}
          onChange={(e) => setMobileNumber(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Confirm Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Confirm password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>
      <Button variant="primary" type="submit" id="submitBtn" disabled={disable}>
        Submit
      </Button>
      {/* {
                isActive ? <Button variant="primary" type="submit" id='submitBtn'>Submit</Button>
                :
                <Button variant="primary" type="submit" id='submitBtn' disabled>Submit</Button>
            } */}
    </Form>
  );
}
