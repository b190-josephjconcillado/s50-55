const usersData = [
    {
        adminId: "62ed1583175c29769e7d9dfa",
        firstName: "Joseph",
        lastName: "Concillado",
        mobileNumber: "+639275001407",
        email: "josephjconcillado@gmail.com",
        password: "123456",
        isAdmin: true
    },
    {
        adminId: "62abd1583172d29769e7d9dfa",
        firstName: "Joseph",
        lastName: "Concillado",
        mobileNumber: "+639275001407",
        email: "admin@gmail.com",
        password: "123456",
        isAdmin: true
    },
]

export default usersData;